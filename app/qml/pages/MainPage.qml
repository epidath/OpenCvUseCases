// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0

Page {
    objectName: "mainPage"
    allowedOrientations: Orientation.All

    PageHeader {
        objectName: "pageHeader"
        title: qsTr("OpenCV Usage")
        extraContent.children: [
            IconButton {
                objectName: "aboutButton"
                icon.source: "image://theme/icon-m-about"
                anchors.verticalCenter: parent.verticalCenter

                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        ]
    }

    Button {
        objectName: "openImageButton"
        anchors.centerIn: parent
        text: qsTr("Open image")
        onClicked: pageStack.push(imagePickerComponent)
    }

    Component {
        id: imagePickerComponent

        ImagePickerPage {
            onSelectedContentChanged: pageStack.push(Qt.resolvedUrl("DisplayImagePage.qml"), {
                    "imagePath": selectedContentProperties.filePath
                })
        }
    }
}
