// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.auroraos.OpenCVUsage 1.0

Page {

    objectName: "mainPage"

    property alias imagePath: openCVImageView.imagePath

    PageHeader {
        id: pageHeader

        objectName: "pageHeader"
        title: imagePath.slice(imagePath.lastIndexOf("/") + 1)
    }

    OpenCVImageView {
        id: openCVImageView

        anchors {
            fill: parent
            margins: Theme.horizontalPageMargin
        }
    }
}
