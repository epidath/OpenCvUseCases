// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QPainter>

#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"

#include "opencvimageview.h"

/*!
 * \brief Constructor.
 * \param parent QQuickItem parent instance.
 */
OpenCVImageView::OpenCVImageView(QQuickItem *parent) : QQuickPaintedItem(parent)
{
    connect(this, &OpenCVImageView::imagePathChanged, this, &OpenCVImageView::updateOpenCVImage);
}

void OpenCVImageView::paint(QPainter *painter)
{
    int imageWidth = m_opencvImage.size().width;
    int imageHeight = m_opencvImage.size().height;
    qreal scale = calculateImageScale(imageWidth, imageHeight);

    QImage image = matToQImage(m_opencvImage)
                           .scaled(imageWidth * scale, imageHeight * scale,
                                   Qt::AspectRatioMode::KeepAspectRatio);
    int x = (width() - image.width()) / 2;
    int y = (height() - image.height()) / 2;
    painter->drawImage(x, y, image);
}

QString OpenCVImageView::imagePath() const
{
    return m_imagePath;
}

void OpenCVImageView::setImagePath(const QString &imagePath)
{
    m_imagePath = imagePath;
    emit imagePathChanged();
}

/*!
 * \brief Updates the OpenCV Mat object with image by the current image path.
 * This method also converts the image colors because the CV reads an image in BGR format whereas
 * Qt in RGB.
 * In the end this method updates the view to repaint it.
 */
void OpenCVImageView::updateOpenCVImage()
{
    m_opencvImage = cv::imread(m_imagePath.toStdString());
    cv::cvtColor(m_opencvImage, m_opencvImage, cv::COLOR_BGR2RGB);
    update();
}

/*!
 * \brief Calculates a scale for the given image size to fit inside the view.
 * \param imageWidth Image width.
 * \param imageHeight Image height.
 * \return Scake value to fit image inside the view.
 */
qreal OpenCVImageView::calculateImageScale(const int imageWidth, const int imageHeight)
{
    qreal xScale = width() / imageWidth;
    qreal yScale = height() / imageHeight;
    return xScale < yScale ? xScale : yScale;
}

/*!
 * \brief Converts the given OpenCV's Mat into QImage.
 * \param mat Mat to convert.
 * \return Converted mat as the QImage object.
 */
QImage OpenCVImageView::matToQImage(cv::Mat &mat)
{
    if (mat.empty())
        return QImage();
    switch (mat.type()) {
    case CV_8UC1:
        return matToQImage(mat, QImage::Format_Indexed8);
    case CV_8UC3:
        return matToQImage(mat, QImage::Format_RGB888);
    case CV_8UC4:
        return matToQImage(mat, QImage::Format_ARGB32);
    }
    return QImage();
}

/*!
 * \brief Converts the given OpenCV's Mat into QImage.
 * \param mat Mat to convert.
 * \param format QImage format.
 * \return Converted mat as the QImage object.
 */
QImage OpenCVImageView::matToQImage(cv::Mat &mat, const QImage::Format format)
{
    return QImage((const uchar *)mat.data, mat.cols, mat.rows, mat.step, format);
}

/*!
 * \brief Converts the given QImage object into OpenCV's Mat.
 * \param image QImage to convert.
 * \return Converted image as the Mat object.
 */
cv::Mat OpenCVImageView::qImageToMat(QImage &image)
{
    if (image.isNull())
        return cv::Mat();
    switch (image.format()) {
    case QImage::Format_Indexed8:
        return qImageToMat(image, CV_8UC1);
    case QImage::Format_RGB888:
        return qImageToMat(image, CV_8UC3);
    case QImage::Format_RGB32:
    case QImage::Format_ARGB32:
    case QImage::Format_ARGB32_Premultiplied:
        return qImageToMat(image, CV_8UC4);
    default:
        break;
    }
    return cv::Mat();
}

/*!
 * \brief Converts the given QImage object into OpenCV's Mat.
 * \param image QImage to convert.
 * \param format OpenCV image format.
 * \return Converted image as the Mat object.
 */
cv::Mat OpenCVImageView::qImageToMat(QImage &image, const int format)
{
    return cv::Mat(image.height(), image.width(), format, image.bits(), image.bytesPerLine());
}
