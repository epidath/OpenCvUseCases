// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef OPENCVIMAGEVIEW_H
#define OPENCVIMAGEVIEW_H

#include <QQuickPaintedItem>
#include <QImage>

#include "opencv2/core.hpp"

/*!
 * \brief OpenCVImageView class is a view that reads an image using OpenCV API and draws the image.
 */
class OpenCVImageView : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString imagePath READ imagePath WRITE setImagePath NOTIFY imagePathChanged)

public:
    explicit OpenCVImageView(QQuickItem *parent = nullptr);
    void paint(QPainter *painter) override;

    QString imagePath() const;
    void setImagePath(const QString &imagePath);

private:
    void updateOpenCVImage();

    qreal calculateImageScale(const int imageWidth, const int imageHeight);

    QImage matToQImage(cv::Mat &mat);
    QImage matToQImage(cv::Mat &mat, const QImage::Format format);
    cv::Mat qImageToMat(QImage &image);
    cv::Mat qImageToMat(QImage &image, const int format);

private:
    QString m_imagePath;
    cv::Mat m_opencvImage;

signals:
    void imagePathChanged();
};

#endif // OPENCVIMAGEVIEW_H
