# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.OpenCVUsage

CONFIG += \
    auroraapp

PKGCONFIG += \

# OpenCV
OPENCV_BUILD_PATH=$$OUT_PWD/../opencv
opencv_library_install.path = /usr/share/ru.auroraos.OpenCVUsage/lib/
opencv_library_install.files = $$OPENCV_BUILD_PATH/lib/*.so*
opencv_library_install.CONFIG = no_check_exist
INSTALLS += opencv_library_install

OPENCV_SOURCE_PATH=$$PWD/../opencv/opencv
INCLUDEPATH += \
    $$OPENCV_BUILD_PATH/include \
    $$OPENCV_BUILD_PATH \
    $$OPENCV_SOURCE_PATH/modules/core/include \
    $$OPENCV_SOURCE_PATH/modules/imgcodecs/include \
    $$OPENCV_SOURCE_PATH/modules/imgproc/include \
    $$OPENCV_SOURCE_PATH/include

LIBS += -L$$OPENCV_BUILD_PATH/lib/ -lopencv_core -lopencv_imgcodecs -lopencv_imgproc

SOURCES += \
    src/main.cpp \
    src/opencvimageview.cpp

HEADERS += \
    src/opencvimageview.h

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n

TRANSLATIONS += \
    translations/ru.auroraos.OpenCVUsage.ts \
    translations/ru.auroraos.OpenCVUsage-ru.ts \
