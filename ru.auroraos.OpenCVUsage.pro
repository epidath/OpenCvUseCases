# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS = app

OTHER_FILES += \
        $$files(rpm/*) \
        AUTHORS.md \
        CODE_OF_CONDUCT.md \
        CONTRIBUTING.md \
        LICENSE.BSD-3-Clause.md \
        README.ru.md \
        README.md
