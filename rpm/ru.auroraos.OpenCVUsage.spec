%define __provides_exclude_from ^%{_datadir}/%{name}/lib/.*$
%define __requires_exclude ^(libjpeg.*|libtiff.*|libwebp.*|libopencv_core.*|libopencv_imgcodecs.*|libopencv_imgproc.*|libjpeg.*|libstdc++.*|libopenjp2.*)$

Name:       ru.auroraos.OpenCVUsage

Summary:    Application template to add and use the OpenCV library
Version:    0.1
Release:    1
Group:      Qt/Qt
License:    BSD-3-Clause
URL:        https://auroraos.ru
Source0:    %{name}-%{version}.tar.bz2

Requires:   sailfishsilica-qt5 >= 0.10.9
BuildRequires:  pkgconfig(auroraapp)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)


%description
Application template to add and use the OpenCV library

%prep
%autosetup

%build

mkdir -p $RPM_BUILD_DIR/opencv

%cmake -B$RPM_BUILD_DIR/opencv \
    -H%{_sourcedir}/../opencv/opencv \
    -S%{_sourcedir}/../opencv/opencv \
    -DCMAKE_SYSTEM_PROCESSOR=%{_arch} \
    -DCMAKE_CXX_COMPILER=/usr/bin/g++ \
    -DCMAKE_MAKE_PROGRAM=/usr/bin/make \
    -DBUILD_TESTS=OFF \
    -DBUILD_PERF_TESTS=OFF \
    -DBUILD_LIST=core,imgcodecs \
    -DWITH_EIGEN=ON \
    -DWITH_VULKAN=ON \
    -DWITH_V4L=OFF \
    -DWITH_FFMPEG=OFF \
    -DWITH_GSTREAMER=OFF \
    -DWITH_MSMF=OFF \
    -DWITH_DSHOW=OFF \
    -DWITH_AVFOUNDATION=OFF \
    -DVIDEOIO_ENABLE_PLUGINS=OFF \
    -DHIGHGUI_ENABLE_PLUGINS=OFF \
    -DWITH_PROTOBUF=OFF \
    -DBUILD_PROTOBUF=OFF \
    -DOPENCV_DNN_OPENCL=OFF \
    -DBUILD_JAVA=OFF \
    -DBUILD_FAT_JAVA_LIB=OFF \
    -DENABLE_PYLINT=OFF \
    -DENABLE_FLAKE8=OFF \
    -DBUILD_opencv_python2=OFF \
    -DBUILD_opencv_python3=OFF \
    -DWITH_CAROTENE=OFF;

cd $RPM_BUILD_DIR/opencv
cmake --build . -j4

cd $RPM_BUILD_DIR

%qmake5

%make_build


%install

%make_install

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%defattr(644,root,root,-)
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
