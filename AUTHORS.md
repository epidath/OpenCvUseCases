# Authors

* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2023
  * Reviewer, 2023
* Konstantin Zvyagin, <k.zvyagin@omp.ru>
  * Maintainer, 2023
  * Developer, 2023
  * Reviewer, 2023
* Andrey Vasilyev
  * Developer, 2023
* Ivan Shchitov
  * Developer, 2023
* Oleg Shevchenko
  * Developer, 2023
  * Reviewer, 2023
* Roman Mazaev
  * Developer, 2023
* Andrey Tretyakov, <a.tretyakov@omp.ru>
  * Reviewer, 2023
* Vladislav Larionov
  * Reviewer, 2023
