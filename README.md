# OpenCV Usage

The application template for demonstrating the connection and usage of the OpenCV library.

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
that allows it to be used in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

## Project Structure

The project has a standard structure of an application based on C++ and QML for Aurora OS.
  
**[app](app)** subproject contains the application source code:
 * **[app.pro](app/app.pro)** file 
   describes the app subproject structure for the qmake build system.
 * **[icons](app/icons)** directory contains the application icons for different screen resolutions.
 * **[qml](app/qml)** directory contains the QML source code and the UI resources.
   * **[cover](app/qml/cover)** directory contains the application cover implementations.
   * **[icons](app/qml/icons)** directory contains the additional custom UI icons.
   * **[pages](app/qml/pages)** directory contains the application pages.
   * **[OpenCVUsage.qml](app/qml/OpenCVUsage.qml)** file
     provides the application window implementation.
 * **[src](app/src)** directory contains the C++ source code.
   * **[main.cpp](app/src/main.cpp)** file is the application entry point.
 * **[translations](app/translations)** directory contains the UI translation files.
 * **[ru.auroraos.OpenCVUsage.desktop](ru.auroraos.OpenCVUsage.desktop)** file
   defines the display and parameters for launching the application.
* **[opencv](opencv)** subproject contains the git submodule with OpenCV library source code.
   * **[opencv](opencv/opencv)** directory is a git submodule with [OpenCV 4.7.0](https://github.com/opencv/opencv/tree/4.7.0).
* **[ru.auroraos.OpenCVUsage.pro](ru.auroraos.OpenCVUsage.pro)** file
 describes the project structure for the qmake build system.
* **[rpm](rpm)** directory contains the rpm-package build settings.
   * **[ru.auroraos.OpenCVUsage.spec](rpm/ru.auroraos.OpenCVUsage.spec)** file is used by rpmbuild tool.

## Compatibility

The project is compatible with all current versions of the Aurora OS.

## Project Building

The project is built in the usual way using the Aurora SDK.

To build the project it needs to increase the Build Engine virtual machine RAM.
It is required to allocate 500 MB RAM to the virtual machine for each processor core.
For example, if Build Engine has 4 processor cores, then its RAM must be at least 2000 MB.

To build OpenCV library it uses CMake. The Cmake running configuration is described inside the [ru.auroraos.OpenCVUsage.spec](rpm/ru.auroraos.OpenCVUsage.spec) file.
The shared libraries and some include files of OpenCV library will be built into the `<build-project-dir>/opencv` directory.
Some include files of OpenCV library can be found in [opencv](opencv/opencv) source directory.
The shared libraries files of OpenCV library will be installed into the `/usr/share/ru.auroraos.OpenCVUsage/lib/` directory on the Aurora OS device.

## Screenshots

![screenshots](screenshots/screenshots.png)


## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
